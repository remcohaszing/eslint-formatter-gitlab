/**
 * @import { ESLint } from 'eslint'
 */

const assert = require('node:assert/strict')
const { existsSync, mkdtempSync, readFileSync, rmSync, writeFileSync } = require('node:fs')
const { tmpdir } = require('node:os')
const { join, resolve } = require('node:path')
const { afterEach, mock, test } = require('node:test')

const chalk = require('chalk')
const { builtinRules } = require('eslint/use-at-your-own-risk')
const yaml = require('yaml')

/** @type {string} */
let tmp

/** @type {ESLint.LintResultData} */
const lintResultData = {
  cwd: '',
  rulesMeta: {}
}
for (const [name, { meta }] of builtinRules.entries()) {
  if (meta) {
    lintResultData.rulesMeta[name] = meta
  }
}

/**
 * Read JSON data from the temporary est workspace
 *
 * @param {string} fileName
 *   The file name to read relative to the temporary test workspace.
 * @returns {unknown}
 *   The parse JSON data.
 */
function readOutput(fileName) {
  return JSON.parse(readFileSync(resolve(tmp, fileName), 'utf8'))
}

/**
 * @param {Record<string, string>} testEnv
 *   A mapping of environment variables to use in the test.
 * @param {object | string} gitlabCI
 *   An object which stubs the content of a `.gitlab-ci.yml` file.
 * @returns {Function}
 *   The patched module.
 */
function setup(testEnv, gitlabCI) {
  process.env = testEnv
  tmp = mkdtempSync(join(tmpdir(), 'eslint-formatter-gitlab-'))
  lintResultData.cwd = tmp
  writeFileSync(
    join(tmp, '.gitlab-ci.yml'),
    typeof gitlabCI === 'string' ? gitlabCI : yaml.stringify(gitlabCI)
  )
  mock.method(process, 'cwd', () => tmp)
  delete require.cache[require.resolve('eslint-formatter-gitlab')]
  return require('eslint-formatter-gitlab')
}

afterEach(() => {
  rmSync(tmp, { recursive: true })
})

test('write a code quality report', () => {
  const formatter = setup(
    { CI_JOB_NAME: 'test-lint-job' },
    { 'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } } }
  )
  formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 1,
        warningCount: 1,
        messages: [
          { line: 42, message: 'Unexpected console statement', ruleId: 'no-console', severity: 2 },
          { line: 43, message: 'Unexpected debugger statement', ruleId: 'no-debugger', severity: 1 }
        ]
      }
    ],
    lintResultData
  )
  assert.deepEqual(readOutput('output.json'), [
    {
      categories: ['Style'],
      check_name: 'no-console',
      content: {
        body: `Disallow the use of \`console\`

[no-console](https://eslint.org/docs/latest/rules/no-console)`
      },
      description: 'Unexpected console statement',
      fingerprint: '104f657895017428ff1fab2f7efd1c08',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    },
    {
      categories: ['Bug Risk', 'Style'],
      check_name: 'no-debugger',
      content: {
        body: `Disallow the use of \`debugger\`

[no-debugger](https://eslint.org/docs/latest/rules/no-debugger)`
      },
      description: 'Unexpected debugger statement',
      fingerprint: 'e43578d636c9324207bb280b255ceffb',
      location: { lines: { begin: 43, end: 43 }, path: 'filename.js' },
      severity: 'minor',
      type: 'issue'
    }
  ])
})

test('generate unique hashes for duplicate messages', () => {
  const formatter = setup(
    { CI_JOB_NAME: 'test-lint-job' },
    { 'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } } }
  )
  formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 2,
        warningCount: 0,
        messages: [
          { line: 42, message: 'Unexpected console statement', ruleId: 'no-console', severity: 2 },
          { line: 45, message: 'Unexpected console statement', ruleId: 'no-console', severity: 2 }
        ]
      }
    ],
    lintResultData
  )
  assert.deepEqual(readOutput('output.json'), [
    {
      categories: ['Style'],
      check_name: 'no-console',
      content: {
        body: `Disallow the use of \`console\`

[no-console](https://eslint.org/docs/latest/rules/no-console)`
      },
      description: 'Unexpected console statement',
      fingerprint: '104f657895017428ff1fab2f7efd1c08',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    },
    {
      categories: ['Style'],
      check_name: 'no-console',
      content: {
        body: `Disallow the use of \`console\`

[no-console](https://eslint.org/docs/latest/rules/no-console)`
      },
      description: 'Unexpected console statement',
      fingerprint: '4a84c9a50fe3a3c8956a26534a9877d3',
      location: { lines: { begin: 45, end: 45 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    }
  ])
})

test('throw if the output location is empty', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job'
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: '' } } }
    }
  )
  assert.throws(
    () => formatter([], lintResultData),
    new TypeError(
      'Expected test-lint-job.artifacts.reports.codequality to be one exact path, got: ""'
    )
  )
})

test('throw if the output location is an array', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job'
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: [] } } }
    }
  )
  assert.throws(
    () => formatter([], lintResultData),
    new TypeError(
      'Expected test-lint-job.artifacts.reports.codequality to be one exact path, got: []'
    )
  )
})

test('don’t fail if a rule id is null', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job'
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } }
    }
  )
  formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 1,
        warningCount: 0,
        messages: [{ line: 42, message: 'This is a linting error', ruleId: null, severity: 2 }]
      }
    ],
    lintResultData
  )
  assert.deepEqual(readOutput('output.json'), [
    {
      categories: ['Style'],
      check_name: '',
      description: 'This is a linting error',
      fingerprint: '8e905c6c557d0066e5a01827b6216be6',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    }
  ])
})

test('don’t fail on partial rule metadata', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job'
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } }
    }
  )
  formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 1,
        warningCount: 0,
        messages: [
          { line: 42, message: 'This is a linting error', ruleId: 'missing-docs', severity: 2 }
        ]
      },
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 1,
        warningCount: 0,
        messages: [
          { line: 42, message: 'This is a linting error', ruleId: 'empty-docs', severity: 2 }
        ]
      },
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 1,
        warningCount: 0,
        messages: [
          { line: 42, message: 'This is a linting error', ruleId: 'no-description', severity: 2 }
        ]
      },
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 1,
        warningCount: 0,
        messages: [{ line: 42, message: 'This is a linting error', ruleId: 'no-url', severity: 2 }]
      }
    ],
    {
      cwd: process.cwd(),
      rulesMeta: {
        'missing-docs': {},
        'empty-docs': {
          docs: {}
        },
        'no-description': {
          docs: { url: 'https://example.com' }
        },
        'no-url': {
          docs: { description: 'Description only' }
        }
      }
    }
  )
  assert.deepEqual(readOutput('output.json'), [
    {
      categories: ['Style'],
      check_name: 'missing-docs',
      description: 'This is a linting error',
      fingerprint: 'e7160d372d13896d48fe5116209bb201',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    },
    {
      categories: ['Style'],
      check_name: 'empty-docs',
      description: 'This is a linting error',
      fingerprint: '892f75d009ce0ded668e1b6bb99c9f55',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    },
    {
      categories: ['Style'],
      check_name: 'no-description',
      content: { body: '[no-description](https://example.com)' },
      description: 'This is a linting error',
      fingerprint: 'b230d69d1534c076b20af576036459b5',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    },
    {
      categories: ['Style'],
      check_name: 'no-url',
      content: { body: 'Description only' },
      description: 'This is a linting error',
      fingerprint: '1dd6e71cf9acd1aef18bf8374e752ab8',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    }
  ])
})

test('don’t fail on missing rule metadata', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job'
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } }
    }
  )
  formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 1,
        warningCount: 0,
        messages: [
          { line: 42, message: 'This is a linting error', ruleId: 'empty-metadata', severity: 2 }
        ]
      },
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 1,
        warningCount: 0,
        messages: [
          { line: 42, message: 'This is a linting error', ruleId: 'missing-metadata', severity: 2 }
        ]
      }
    ],
    {
      cwd: process.cwd(),
      rulesMeta: {
        'missing-metadata': undefined
      }
    }
  )
  assert.deepEqual(readOutput('output.json'), [
    {
      categories: ['Style'],
      check_name: 'empty-metadata',
      description: 'This is a linting error',
      fingerprint: 'e6b959c75e6de0e281ddcb3adc80451f',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    },
    {
      categories: ['Style'],
      check_name: 'missing-metadata',
      description: 'This is a linting error',
      fingerprint: 'a3276e730efbabd780f1ee8121f335d0',
      location: { lines: { begin: 42, end: 42 }, path: 'filename.js' },
      severity: 'major',
      type: 'issue'
    }
  ])
})

test('skip the output if CI_JOB_NAME is not defined', () => {
  const formatter = setup(
    {},
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } }
    }
  )
  formatter([], lintResultData)
  assert.equal(existsSync(join('output.json')), false)
})

test('respect the ESLINT_CODE_QUALITY_REPORT environment variable', () => {
  const outpath = resolve(tmpdir(), 'eslint-formatter-gitlab-test.json')
  const formatter = setup(
    {
      ESLINT_CODE_QUALITY_REPORT: outpath
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } }
    }
  )
  formatter([], lintResultData)
  assert.deepEqual(readOutput(outpath), [])
})

test('provide a report with URLs', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      CI_PROJECT_URL: 'https://gitlab.example.com/test/project',
      CI_COMMIT_SHORT_SHA: 'abc12345ef'
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } }
    }
  )
  const result = formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 1,
        errorCount: 2,
        warningCount: 2,
        messages: [
          {
            line: 1,
            column: 1,
            message: 'Parsing error',
            ruleId: null,
            fatal: true,
            severity: 2
          },
          {
            line: 42,
            column: 3,
            message: 'This is a linting error',
            ruleId: 'linting-error',
            severity: 2
          },
          {
            line: 1,
            column: 1,
            endLine: 1,
            message: 'This is a linting warning',
            ruleId: 'linting-warning',
            severity: 1
          },
          {
            line: 1,
            column: 1,
            endLine: 2,
            message: 'This is a multiline warning',
            ruleId: 'linting-warning',
            severity: 1
          }
        ]
      }
    ],
    lintResultData
  )

  assert.equal(
    result,
    `
${chalk.magenta('fatal')}                   Parsing error                ${chalk.blue(
      'https://gitlab.example.com/test/project/-/blob/abc12345ef/filename.js#L1'
    )}
${chalk.red('error')}  linting-error    This is a linting error      ${chalk.blue(
      'https://gitlab.example.com/test/project/-/blob/abc12345ef/filename.js#L42'
    )}
${chalk.yellow('warn')}   linting-warning  This is a linting warning    ${chalk.blue(
      'https://gitlab.example.com/test/project/-/blob/abc12345ef/filename.js#L1'
    )}
${chalk.yellow('warn')}   linting-warning  This is a multiline warning  ${chalk.blue(
      'https://gitlab.example.com/test/project/-/blob/abc12345ef/filename.js#L1-2'
    )}

${chalk.red('✖')} 4 problems (1 fatal, 1 error, 2 warnings)
`
  )
})

test('don’t try to create URLs when CI environment vars are not set', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job'
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } }
    }
  )
  const result = formatter(
    [
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 1,
        warningCount: 0,
        messages: [
          {
            line: 42,
            column: 1,
            message: 'This is a linting error',
            ruleId: 'linting-error',
            severity: 2
          }
        ]
      },
      {
        filePath: join(process.cwd(), 'filename.js'),
        fatalErrorCount: 0,
        errorCount: 0,
        warningCount: 1,
        messages: [
          {
            line: 1,
            column: 42,
            message: 'This is a linting error',
            ruleId: 'linting-error',
            severity: 2
          }
        ]
      }
    ],
    lintResultData
  )

  assert.equal(
    result,
    `
${chalk.red('error')}  linting-error  This is a linting error  ${tmp}/filename.js:42:1
${chalk.red('error')}  linting-error  This is a linting error  ${tmp}/filename.js:1:42

${chalk.red('✖')} 2 problems (0 fatal, 1 error, 1 warning)
`
  )
})

test('print a message when results are empty', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job'
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } }
    }
  )
  const result = formatter([], lintResultData)

  assert.equal(
    result,
    `
${chalk.green('✔')} No problems found
`
  )
})

test('throw if CI_CONFIG_PATH is empty', () => {
  const formatter = setup(
    {
      CI_CONFIG_PATH: '',
      CI_JOB_NAME: 'test-lint-job'
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } }
    }
  )
  assert.throws(
    () => formatter([], lintResultData),
    new Error(
      'Could not resolve .gitlab-ci.yml to automatically detect report artifact path.' +
        ' Please manually provide a path via the ESLINT_CODE_QUALITY_REPORT variable.'
    )
  )
})

test('don’t fail on !reference', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job'
    },
    `
test-lint-job:
  artifacts:
    reports:
      codequality: output.json

misc: !reference [test-lint-job]
`
  )
  const result = formatter([], lintResultData)
  assert.equal(result, `\n${chalk.green('✔')} No problems found\n`)
})
